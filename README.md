The Google Analytics (GA) Measurement API Client offers full-scope access to the GA Measurement API in Java for web,
standalone applications, and middleware.