package com.coherentlogic.gama.client.core.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

@Entity
@Table(name=GoogleAnalyticsResponseBean.GOOGLE_ANALYTICS_RESPONSE_TABLE)
@Visitable
public class GoogleAnalyticsResponseBean extends SerializableBean {

    private static final long serialVersionUID = 1531755828875318342L;

    static final String GOOGLE_ANALYTICS_RESPONSE_TABLE = "googleAnalyticsResponse", RESPONSE = "response";

    private String response;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {

        String oldValue = this.response;

        this.response = response;

        firePropertyChange(RESPONSE, oldValue, response);
    }

    @Override
    public String toString() {
        return "GoogleAnalyticsResponseBean [response=" + response + "]";
    }
}
