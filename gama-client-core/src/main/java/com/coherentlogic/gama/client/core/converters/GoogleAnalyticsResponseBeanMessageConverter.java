package com.coherentlogic.gama.client.core.converters;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import com.coherentlogic.coherent.data.adapter.core.exceptions.MethodNotSupportedException;
import com.coherentlogic.gama.client.core.domain.GoogleAnalyticsResponseBean;

public class GoogleAnalyticsResponseBeanMessageConverter implements HttpMessageConverter<GoogleAnalyticsResponseBean> {

    @Override
    public boolean canRead(Class<?> type, MediaType mediaType) {
        return
            GoogleAnalyticsResponseBean.class.equals(type)
            &&
            (MediaType.TEXT_PLAIN.equals(mediaType) || MediaType.IMAGE_GIF.equals(mediaType));
    }

    @Override
    public boolean canWrite(Class<?> type, MediaType mediaType) {
        throw new MethodNotSupportedException("The canWrite method is not supported.");
    }

    @Override
    public List<MediaType> getSupportedMediaTypes() {
        return new ArrayList<MediaType> (Arrays.asList(MediaType.TEXT_PLAIN, MediaType.IMAGE_GIF));
    }

    @Override
    public GoogleAnalyticsResponseBean read(Class<? extends GoogleAnalyticsResponseBean> type, HttpInputMessage message)
        throws IOException, HttpMessageNotReadableException {

        GoogleAnalyticsResponseBean result = new GoogleAnalyticsResponseBean ();

        String body = IOUtils.toString(message.getBody(), Charset.defaultCharset());

        result.setResponse(body);

        return result;
    }

    @Override
    public void write(GoogleAnalyticsResponseBean arg0, MediaType mediaType, HttpOutputMessage arg2)
        throws IOException, HttpMessageNotWritableException {
        throw new MethodNotSupportedException("The write method is not supported.");
    }
}
